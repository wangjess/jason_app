import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class GalleriesService{

    constructor(){}


    getGalleries() {
        const url = `${API_URL}/api/galleries/`;
        return axios.get(url).then(response => response.data);
    }  
    getGalleriesByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data);
    }
    getGallery(pk) {
        const url = `${API_URL}/api/gallery/${pk}`;
        return axios.get(url).then(response => response.data);
    }
    deleteGallery(gallery){
        const url = `${API_URL}/api/gallery/${gallery.pk}`;
        return axios.delete(url);
    }
    createGallery(gallery){
        const url = `${API_URL}/api/galleries/`;
        return axios.post(url,gallery);
    }
    updateGallery(gallery){
        const url = `${API_URL}/api/gallery/${gallery.pk}`;
        return axios.put(url,gallery);
    }
}