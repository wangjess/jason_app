from django.db import models

# Create your models here.
class Gallery(models.Model):
    name = models.CharField("Gallery name", max_length=255)
    owner_login = models.EmailField()
    owner_id = models.IntegerField()
    shareable = models.BooleanField()

    def __str__(self):
        return self.name