from rest_framework import serializers
from .models import Gallery

class GallerySerializer(serializers.ModelSerializer):

    class Meta:
        model = Gallery 
        fields = ('pk','name', 'owner_login', 'owner_id', 'shareable')