"""djangoreactproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from galleries import views as galleries_views
from categories import views as categories_views
from django.conf.urls import url

urlpatterns = [
    # Added dummy path
    path('users/', include('users.urls')),
    # ROOT SHIT
    path('admin/', admin.site.urls),
    # User paths
    path('users/delete', include('users.urls')),
    path('users/edit', include('users.urls')),
    ## Photo paths
    path('photos/', include('photos.urls')),
    path('photos/delete', include('photos.urls')),
    path('photos/edit', include('photos.urls')),

    ## Galleries paths 
    url('galleries/', galleries_views.galleries_list),
    url('gallery/(?P<pk>[0-9]+)', galleries_views.galleries_detail),

    #comments!

    url('categories/', categories_views.categories_list),
    url('category/(?P<pk>[0-9]+)', categories_views.categories_detail),
    #comments!

]