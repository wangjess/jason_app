from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Category 
from .serializers import *

@api_view(['GET', 'POST'])
def categories_list(request):
	"""
	GET = List categories.
	POST = Create new category.
	"""
	if request.method == 'GET':
		data = []
		nextPage = 1
		previousPage = 1
		galleries = Category.objects.all()
		page = request.GET.get('page', 1)
		paginator = Paginator(galleries, 10)
		try:
			data = paginator.page(page)
		except PageNotAnInteger:
			data = paginator.page(1)
		except EmptyPage:
			data = paginator.page(paginator.num_pages)

		serializer = CategorySerializer(data,context={'request': request} ,many=True)
		if data.has_next():
			nextPage = data.next_page_number()
		if data.has_previous():
			previousPage = data.previous_page_number()

		return Response({'data': serializer.data , 'count': paginator.count, 'numpages' : paginator.num_pages, 'nextlink': '/api/categories/?page=' + str(nextPage), 'prevlink': '/api/categories/?page=' + str(previousPage)})
	
	if request.method == 'POST':
		serializer = CategorySerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def categories_detail(request, pk):
	"""
	DELETE = Delete category.
	"""
	try:
		category = Category.objects.get(pk=pk)
	except Category.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'DELETE':
		category.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)