from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField("Category name", max_length=255)
    gallery_id = models.IntegerField()

    def __str__(self):
        return self.name